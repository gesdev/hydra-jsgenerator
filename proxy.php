<?php
require_once "JSGenerator.php";
$jsGenerator = new JSGenerator();
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST');  
header('Content-type: application/json');
if (isset($_REQUEST["uri"])) {
  echo $jsGenerator->get_contents($_REQUEST["uri"], $_SERVER["REQUEST_METHOD"],$_REQUEST);
}
?>