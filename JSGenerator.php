<?php
require("../hydra-client/vendor/autoload.php");
use ML\HydraClient\Client as HydraClient;
use ML\JsonLD\JsonLD;

class JSGenerator {
  protected $server_path;
  protected $entrypoint;
  protected $vocab_url;
  protected $graph;
  protected $types_discovered;
  protected $proxy_url;
  public function get_contents($url = "", $method = "GET", $post_params = array()) {
    if (empty($url))
      return json_encode(array(),true);
    if (!preg_match("/^http/",$url)) {
      $url = $this->server_path.$url; 
    } 
    
    $curl = curl_init();
    curl_setopt($curl,CURLOPT_URL,$url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_TIMEOUT, '3');
    curl_setopt($curl, CURLOPT_FAILONERROR, 1);
    
    if (strtoupper($method) == "POST") {
      curl_setopt($curl, CURLOPT_POST, 1);
      curl_setopt($curl, CURLOPT_POSTFIELDS, $post_params);
    }
    
    $content = trim(curl_exec($curl));
    $curl_info = curl_getinfo($curl);

    if (!preg_match("#application\/(ld\+)?json#",$curl_info["content_type"])) {
      /* error_log($curl_info["content_type"]);
      error_log($url. "  ".$method);
      error_log($content); */
      $content = "";
    }
    
    curl_close($curl);
    return $content;
  }
  
  function __construct($entrypoint = "") {
    if (!empty($entrypoint)) {
      $this->types_discovered = array();
      $entry_point_url = parse_url($entrypoint);
      $this->server_path = $entry_point_url["scheme"]."://".$entry_point_url["host"];
      $this->server_path .= (isset($entry_point_url["port"])) ? ":".$entry_point_url["port"] : "";
      $this->entrypoint = $entrypoint;
      $entry = JSGenerator::get_contents($this->entrypoint);

      if (!empty($entry)) {
	$docroot = JsonLD::compact($this->get_contents($this->entrypoint),null,(object)array("base" => $this->server_path));
	if (isset($docroot->{"@type"})) {
	  $this->vocab_url = $docroot->{"@type"};
	  $vocab_url = explode("#",$docroot->{"@type"});
	  $vocab = json_decode(JSGenerator::get_contents($vocab_url[0]),true);
	  if (isset($vocab["supportedClasses"])) {
	    $this->graph = $vocab["supportedClasses"];
	  }
	} else {
	  $this->graph = array();
	  $this->vocab_url = "";
	}
      }      
    }
    $this->proxy_url = "/cis/communication-layer/proxy.php";
  }
  
  function getElementById($id) {
    if (empty($this->graph)) return;
    foreach ($this->graph as $element) {
      if (isset($element["@id"]) && $element["@id"] == $id)
	return $element;
    }
  }
  
  function getElementsByDomain($domain) {
    if (empty ($this->graph)) {
      return array();
    }
    $elements = array();
    foreach ($this->graph as $element) {
      if (isset($element["domain"]) && $element["domain"] == $domain)
	array_push($elements, $element);
    }
    return $elements;
  }

  function makeAjaxRequestor($function_params = array(), $method = "GET",$request_params = array()) {
    $proxy_url = "/cis/communication-layer/proxy";
    $request_params_str = "";
    $jsRequest = "";
    foreach ($request_params as $key => $val) {
      $request_params_str .= "\"${key}=\"+${val}";
      if ($request_params[$key] != end($request_params)) {
	$request_params_str .= "+\"&\"+";
      }
    }
    $jsRequest .= "params = ". ((empty($request_params_str)) ? "\"\"" : $request_params_str ) .";\n";
    $jsRequest .= "\t\tdoAjax(\"". $this->proxy_url."\",\"${method}\",callback,params);";
    return $jsRequest;
  }

  function exploreOperations ($object, $property, $operations, $namespace = "", $is_entrypoint = false) {
    $type = (isset ($object->{"@type"}[0])) ? $object->{"@type"}[0] : "";
    $object_desc = $this->getElementById($type);
    $functions = "";    
    $namespace = (empty($namespace)) ? $namespace : $namespace.".";
    if ($is_entrypoint) {
      $uri = (isset($object->{$property["property"]["@id"]}[0]->{"@id"})) ? $object->{$property["property"]["@id"]}[0]->{"@id"} : null;
      if (empty($uri)) {
	$uri = (isset($property["property"]["@id"])) ? $property["property"]["@id"] : null;
      }
    } else {
      $uri = null;
    }
      
    foreach ($operations as $operation) {
      $function_params = array();
      $request_params = array();      
     
      if ($uri == "uri" or empty($uri)) {
	$function_params["uri"] = "uri";
	$request_params["uri"] = "uri";
      } else {
	$request_params["uri"] = "\"".$uri."\"";
      }
      
      $method = (isset($operation["method"])) ? $operation["method"] : "GET";
      $expects = (isset($operation["expects"])) ? $operation["expects"] : null;
      $expects_desc = (!empty($expects)) ? $this->getElementById($expects) : array();
      if (isset($expects_desc["supportedProperties"])) {
	foreach ($expects_desc["supportedProperties"] as $expects_param) {
	  if ($method != "GET") {
	    if (!isset($expects_param["readonly"]) || isset($expects_param["writeonly"]) && $expects_param["readonly"] == false) {
	      $function_params[$expects_param["property"]["label"]] = $expects_param["property"]["label"];
	      $request_params[$expects_param["property"]["label"]] = $expects_param["property"]["label"];
	    }
	  }
	}
      }
      
      $function_params["callback"] = "callback";
      
      $functions .= "\n\t//".$operation["label"];
      $functions .= "\n\t${namespace}".preg_replace("/^_:(.+)/","$1",$operation["@id"])." = function(".implode($function_params,",").") {\n";
      $functions .= "\t\t".$this->makeAjaxRequestor($function_params,$method,$request_params)."\n\t}\n";
      $url_to_search = isset($object->{$property["property"]["@id"]}[0]->{"@id"}) ? $object->{$property["property"]["@id"]}[0]->{"@id"} : null;
      
      if (!empty($url_to_search)) {
	$doc = $this->get_contents($url_to_search,$method);
	if (!empty($doc)) {
	  $doc = JsonLD::expand($doc,(object)array("base" => $this->getServerPath()));
	  $functions .= $this->makeJSFunction($namespace,$doc);
	}
      }
    }
    return $functions;
  }
  
  function exploreProperties ($object,$namespace = "", $is_entrypoint = false) {
    $type = (isset ($object->{"@type"}[0])) ? $object->{"@type"}[0] : "";
    $object_desc = $this->getElementById($type);    
    $properties = (isset ($object_desc["supportedProperties"])) ? $object_desc["supportedProperties"] : array();
    
    if (is_array($properties) && empty($properties)) 
      return "";
    
    $result = "";
    
    foreach ($properties as $property) {
      if (!is_array($property)) {
	continue;
      }
      $operations = (isset($property["property"]["supportedOperations"])) ? $property["property"]["supportedOperations"] : array();
      $result .= $this->exploreOperations($object,$property, $operations,$namespace,$is_entrypoint);
    }

    $operations = (isset ($object_desc["supportedOperations"])) ? $object_desc["supportedOperations"] : array();    
    $result .= $this->exploreOperations($object,array("property" => (array)$object), $operations,$namespace,$is_entrypoint);
    
    return $result;
  }


  function makeJSFunction($namespace = "", $objects , $is_entrypoint = false) {
    if (!sizeof((array) $objects))
      return "";
    
    $functions = "";    
    
    foreach ($objects as $object) {
      $type = (isset ($object->{"@type"}[0])) ? $object->{"@type"}[0] : "";
      $object_desc = $this->getElementById($type);
      
      if (isset ($object->{"http://purl.org/hydra/core#members"})) {
	$members = $object->{"http://purl.org/hydra/core#members"};
	if (sizeof($members)) {
	  $first_member = $members[0];
	  $type_member = (isset($first_member->{"@type"})) ? $first_member->{"@type"} : null;
	  
	  if(!empty($type_member) && !array_search($type_member[0], $this->types_discovered)) {
	    $doc_in = JsonLD::expand($this->get_contents($first_member->{"@id"},"GET"),(object)array("base" => $this->getServerPath()));
	    $functions .= $this->makeJSFunction($namespace,$doc_in);
	  }
	}
	continue;
      }
      
      //Verifica si el @type fue ya explorado.
      if (array_search($type,$this->types_discovered)) {
	continue;
      }
      
      //Notifica que este @type ya no debe ser explorado si se vuelve a encontrar.
      array_push($this->types_discovered, $type);
      
      $namespace = ($is_entrypoint) ? $namespace."." : $namespace;

      if (isset ($object_desc["label"])) {
	$functions .= "\n\t".$namespace.$object_desc["label"]." = {};";
      }
      $functions .= $this->exploreProperties($object, $namespace.$object_desc["label"], $is_entrypoint);
    }
    return $functions;
  }
  
  function getGraph() {
    return $this->graph;
  }

  function getServerPath() {
    return $this->server_path;
  }    
}
?>